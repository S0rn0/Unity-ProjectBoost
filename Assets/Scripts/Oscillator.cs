﻿using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] Vector3 movementVector = new Vector3(10f, 10f, 10f);
    [SerializeField] float period = 2f;

    Vector3 startingPosition;

	void Start ()
    {
        startingPosition = transform.position;
    }
	
	void Update ()
    {
        if (period <= Mathf.Epsilon) return;
        transform.position = startingPosition + ((Mathf.Sin(Time.time / period * Mathf.PI * 2f) / 2f) + 0.5f) * movementVector;
    }
}
